# cs7ds4

Data Visualization Assignments completed as part of the M.Sc in Computer Science, Trinity College, in 2020.

 - `01_snow`: reproduction of Snow's cholera map.
 - `02_nightingale`: reproduction of Nightingale's diagrams of the causes of patient mortality.
 - `03_minard`: reproduction of Minard's visualization of Napoleon's retreat.

