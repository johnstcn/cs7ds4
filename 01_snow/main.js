(function() {  
    var svg = d3.select('svg');
    var bgImg = svg.append('image')
        .attr('xlink:href', './mapclean.jpg')
        .attr('width', 1000)
        .attr('height', 911);
    d3.csv("./snow_pixelcoords.csv").then(handleData);

    function handleData(data) {
        svg.selectAll('.symbol-circle')
            .data(data.filter(d => d.count > 0))
            .enter()
            .append('circle')
            .attr('cx', d => d.x_screen)
            .attr('cy', d => d.y_screen)
            .attr('r', d => 2 * Math.log(d.count))
            .style('fill', 'red')
            .exit();
        svg.selectAll('.symbol-cross')
            .data(data.filter(d => d.count < 0))
            .enter()
            .append('path')
            .attr('transform', function(d) {
                return 'translate(' + d.x_screen + ',' + d.y_screen + ')';
            })
            .attr('d', d3.symbol().type(d3.symbolCross))
            .style('fill', 'steelblue')
            .exit();
    }
})();